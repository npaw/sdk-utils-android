package com.npaw.utils

import java.io.PrintWriter
import java.io.StringWriter

/** Provides a set of convenient methods to ease the logging. */
object Log {

    enum class Level constructor(val level: Int) {
        SILENT(6),
        ERROR(5),
        WARNING(4),
        /** Console will show notices like lifecycle logs. */
        NOTICE(3),
        /** Console will show debug messages like player events. */
        DEBUG(2),
        /** Console will show verbose messages like HTTP Requests. */
        VERBOSE(1);
    }

    /** Tag for logging with the [android.util.Log] class. */
    private const val TAG = "NPAW"

    /** Only logs of this importance or higher will be shown. */
    @JvmStatic var level = Level.ERROR

    /**
     * Prints a message using Android's [Log] class. The message is only logged if the
     * [currentLogLevel] is lower or equal than the [logLevel].
     */
    internal fun reportLogMessage(logLevel: Level, message: String) {
        if (level.level <= logLevel.level) {
            when (logLevel) {
                Level.ERROR -> android.util.Log.e(TAG, message)
                Level.WARNING -> android.util.Log.w(TAG, message)
                Level.NOTICE -> android.util.Log.i(TAG, message)
                Level.DEBUG -> android.util.Log.d(TAG, message)
                Level.VERBOSE -> android.util.Log.v(TAG, message)
                else -> Unit
            }
        }
    }

    /** Logs the exception and prints its stack trace. */
    fun error(exception: Exception) {
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        exception.printStackTrace(pw)
        error(sw.toString())
    }

    fun error(message: String) { reportLogMessage(Level.ERROR, message) }
    fun warn(message: String) { reportLogMessage(Level.WARNING, message) }
    fun notice(message: String) { reportLogMessage(Level.NOTICE, message) }
    fun debug(message: String) { reportLogMessage(Level.DEBUG, message) }
    fun verbose(message: String) { reportLogMessage(Level.VERBOSE, message) }
}

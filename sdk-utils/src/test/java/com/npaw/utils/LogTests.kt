package com.npaw.utils

import junit.framework.TestCase.assertNotNull

import org.junit.Test
import org.junit.runner.RunWith

import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class LogTests {

    @Test
    fun reportLogMessage() {
        assertNotNull(Log)
        Log.level = Log.Level.VERBOSE
        Log.reportLogMessage(Log.Level.SILENT, "")
        Log.error(Exception())
        Log.error("")
        Log.warn("")
        Log.notice("")
        Log.debug("")
        Log.verbose("")

        Log.level = Log.Level.SILENT
        Log.error(Exception())
    }
}